package com.szendo.mydesk.model.auth

import kotlinx.serialization.Serializable

@Serializable
data class SnapHqSigninResponse(val accessToken: String)
