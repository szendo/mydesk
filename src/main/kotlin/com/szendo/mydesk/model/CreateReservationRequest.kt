package com.szendo.mydesk.model

import com.szendo.mydesk.serializer.OffsetDateTimeSerializer
import kotlinx.serialization.Serializable
import java.time.OffsetDateTime

@Serializable
data class CreateReservationRequest(
    val name: String,
    @Serializable(with = OffsetDateTimeSerializer::class)
    val startDateTime: OffsetDateTime,
    @Serializable(with = OffsetDateTimeSerializer::class)
    val endDateTime: OffsetDateTime,
    val desks: List<IdRef>
)
