package com.szendo.mydesk.model

import com.szendo.mydesk.serializer.OffsetDateTimeSerializer
import kotlinx.serialization.Serializable
import java.time.OffsetDateTime

@Serializable
data class Reservation(
    val id: String,
    val name: String,
    val userEmail: String,
    @Serializable(with = OffsetDateTimeSerializer::class)
    val startDateTime: OffsetDateTime,
    @Serializable(with = OffsetDateTimeSerializer::class)
    val endDateTime: OffsetDateTime,
    val zone: Zone,
    val station: Station,
    val desks: List<Desk>
)
