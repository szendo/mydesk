package com.szendo.mydesk.model

import kotlinx.serialization.Serializable

@Serializable
data class IdRef(
    val id: String,
)
