package com.szendo.mydesk.model

import kotlinx.serialization.Serializable

@Serializable
data class Zone(
    val id: String,
    val name: String,
)
