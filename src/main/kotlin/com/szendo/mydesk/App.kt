package com.szendo.mydesk

import com.szendo.mydesk.config.Config
import com.szendo.mydesk.service.GoogleApi
import com.szendo.mydesk.service.SnapHqApi
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import java.io.FileInputStream
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.temporal.ChronoField
import java.time.temporal.TemporalAdjusters

private val zoneId = ZoneId.of("Europe/Budapest")

@OptIn(ExperimentalSerializationApi::class)
fun main() {
    val (apiUrl, clientId, refreshToken, deskId, reservationPrefixes, reservationText) = FileInputStream(
        System.getenv("CONFIG_FILE") ?: "config.json"
    ).use { Json.decodeFromStream<Config>(it) }

    val googleApi = GoogleApi(refreshToken, clientId)
    val snapHqApi = SnapHqApi(googleApi, apiUrl)

    val newEndDate = LocalDate.now().plusDays(29).let { if (it.dayOfWeek == DayOfWeek.SUNDAY) it.minusDays(1) else it }
    val startDate = newEndDate.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY))
    val startDateTime = startDate.atStartOfDay(zoneId).toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC)
    val newEndDateTime = newEndDate.plusDays(1).atStartOfDay(zoneId).minusMinutes(1).toOffsetDateTime()
        .withOffsetSameInstant(ZoneOffset.UTC)

    val existingReservations = snapHqApi.getMyReservations()
    val reservationToUpdate =
        existingReservations.firstOrNull { it.startDateTime.isEqual(startDateTime) && it.desks.any { desk -> desk.id == deskId } }

    val reservation = if (reservationToUpdate == null) {
        val reservationName = if (reservationPrefixes.isNotEmpty()) {
            (startDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR) % reservationPrefixes.size).let {
                "${reservationPrefixes[it]} $reservationText"
            }
        } else {
            reservationText
        }
        println("Creating new reservation: ${startDate..newEndDate}")
        snapHqApi.createReservation(reservationName, startDateTime, newEndDateTime, deskId)
    } else {
        if (newEndDateTime.isAfter(reservationToUpdate.endDateTime)) {
            println("Updating reservation: ${startDate..newEndDate}")
            snapHqApi.updateReservation(reservationToUpdate.id, reservationToUpdate.name, startDateTime, newEndDateTime)
        } else {
            println("Already reserved: ${startDate..newEndDate}")
            null
        }
    }

    reservation?.let { println("Result: ${Json.encodeToString(it)}") }
}
