package com.szendo.mydesk.service

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import java.time.Instant
import java.util.*
import java.util.function.Supplier
import kotlin.reflect.KProperty

abstract class ApiBase {
    protected val httpClient = OkHttpClient()
    protected val jsonMediaType = "application/json".toMediaType()

    protected inline fun <reified T> T.toJsonRequestBody() = Json.encodeToString(this).toRequestBody(jsonMediaType)

    @OptIn(ExperimentalSerializationApi::class)
    protected inline fun <reified T> Response.extractResponse() =
        if (isSuccessful) Json.decodeFromStream<T>(body!!.byteStream())
        else error("Unsuccessful call: ${request.method} ${request.url}, status: $code $message)")

    protected class JWT(private val fetchJwt: Supplier<String>) {
        private var value: String = ""
        private var expiration = Instant.EPOCH

        operator fun getValue(_thisRef: Any, _property: KProperty<*>): String {
            if (Instant.now().isAfter(expiration)) {
                val jwt = fetchJwt.get()
                expiration = extractExpiration(jwt)
                value = jwt
            }
            return value
        }

        private fun extractExpiration(jwt: String): Instant {
            val payload = jwt.split('.', limit = 3)[1]
                .let { Base64.getDecoder().decode(it).decodeToString() }
                .let { Json.parseToJsonElement(it) }
            val exp = payload.jsonObject["exp"]!!.jsonPrimitive.long
            return Instant.ofEpochSecond(exp)
        }
    }
}
