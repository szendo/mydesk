package com.szendo.mydesk.service

import com.szendo.mydesk.model.CreateReservationRequest
import com.szendo.mydesk.model.IdRef
import com.szendo.mydesk.model.Reservation
import com.szendo.mydesk.model.UpdateReservationRequest
import com.szendo.mydesk.model.auth.SnapHqSigninRequest
import com.szendo.mydesk.model.auth.SnapHqSigninResponse
import okhttp3.Request
import java.time.OffsetDateTime

class SnapHqApi(private val googleApi: GoogleApi, private val snapHqApiUrl: String) : ApiBase() {
    private val accessToken: String by JWT {
        httpClient.newCall(
            Request.Builder()
                .url("$snapHqApiUrl/api/authentication/google-sign-in")
                .post(SnapHqSigninRequest(googleApi.idToken).toJsonRequestBody())
                .build()
        ).execute().use { it.extractResponse<SnapHqSigninResponse>() }.accessToken
    }

    fun getMyReservations(): List<Reservation> = httpClient.newCall(
        Request.Builder()
            .url("$snapHqApiUrl/api/workspace/my-reservations")
            .header("authorization", "Bearer $accessToken")
            .get()
            .build()
    ).execute().use { it.extractResponse() }

    fun createReservation(
        name: String, startDateTime: OffsetDateTime, endDateTime: OffsetDateTime, deskId: String
    ): Reservation = httpClient.newCall(
        Request.Builder()
            .url("$snapHqApiUrl/api/workspace/my-reservations")
            .header("authorization", "Bearer $accessToken")
            .post(CreateReservationRequest(name, startDateTime, endDateTime, listOf(IdRef(deskId))).toJsonRequestBody())
            .build()
    ).execute().use { it.extractResponse() }

    fun updateReservation(
        id: String, name: String, startDateTime: OffsetDateTime, endDateTime: OffsetDateTime
    ): Reservation = httpClient.newCall(
        Request.Builder()
            .url("$snapHqApiUrl/api/workspace/my-reservations/$id")
            .header("authorization", "Bearer $accessToken")
            .put(UpdateReservationRequest(name, startDateTime, endDateTime).toJsonRequestBody())
            .build()
    ).execute().use { it.extractResponse() }
}
