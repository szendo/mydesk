package com.szendo.mydesk.model

import kotlinx.serialization.Serializable

@Serializable
data class Desk(
    val id: String,
    val name: String,
    val stationId: String,
)
