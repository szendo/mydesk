package com.szendo.mydesk.model.auth

import kotlinx.serialization.Serializable

@Serializable
data class SnapHqSigninRequest(val idToken: String)
