package com.szendo.mydesk.service

import com.szendo.mydesk.model.auth.GoogleTokenResponse
import okhttp3.FormBody
import okhttp3.Request

class GoogleApi(private val refreshToken: String, private val clientId: String) : ApiBase() {
    val idToken: String by JWT {
        httpClient.newCall(
            Request.Builder()
                .url("https://oauth2.googleapis.com/token")
                .post(
                    FormBody.Builder()
                        .add("grant_type", "refresh_token")
                        .add("client_id", clientId)
                        .add("refresh_token", refreshToken)
                        .build()
                )
                .build()
        ).execute().use { it.extractResponse<GoogleTokenResponse>() }.idToken
    }
}
