package com.szendo.mydesk.config

import kotlinx.serialization.Serializable

@Serializable
data class Config(
    val apiUrl: String,
    val clientId: String,
    val refreshToken: String,
    val deskId: String,
    val reservationPrefixes: List<String> = listOf(),
    val reservationText: String = "Új foglalás",
)
