package com.szendo.mydesk.model

import kotlinx.serialization.Serializable

@Serializable
data class Station(
    val id: String,
    val name: String,
    val numberOfDesks: Int,
)
